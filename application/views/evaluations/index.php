<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/css/inputmask.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>

<style>
  [v-cloak], [v-cloak] > * {
    display: none;
  }

  #levelMessageContainer {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  #levelMessageContainer > p {
    margin: 0;
  }
  
  input[type=number] {
    text-align: center;
  }

</style>

<div id="app">
  <v-app>
  <v-container>
    <h2 v-cloak class="text-xs-center">Listado de evaluaciones</h2>
    <v-layout>
      <v-flex>
        <v-dialog v-model="dialog" persistent max-width="800px">
          <v-btn v-cloak slot="activator" color="222222" dark class="mb-2">Añadir evaluacion</v-btn>
          <v-card>
            <v-card-title>
              <span v-cloak class="headline">{{ editmode ? 'Editar evaluación' : 'Nueva evaluación' }}</span>
              <v-spacer></v-spacer>
              <v-icon v-cloak @click="dialog = false">
                close
              </v-icon>
            </v-card-title>

            <v-card-text>
              <v-container>
                <v-layout wrap>
                <v-form ref="form" style="display: contents">
                  <v-flex xs12>
                    <v-autocomplete :disabled="editmode" :rules="[rules.required]" v-model="newItem.worker_id" item-text="name" item-value="id" :items="selectWorkers" label="Trabajador"></v-autocomplete>
                  </v-flex>
                  <v-flex xs12>
                    <v-menu
                      v-model="menu"
                      :close-on-content-click="false"
                      :nudge-right="40"
                      lazy
                      transition="scale-transition"
                      offset-y
                      full-width
                      min-width="290px"
                    >
                      <template v-slot:activator="{ on }">
                        <v-text-field
                          v-model="newItem.date"
                          label="Fecha de evaluación"
                          readonly
                          v-on="on"
                        ></v-text-field>
                      </template>
                      <v-date-picker :max="moment().format('YYYY-MM-DD')" locale="es" v-model="newItem.date" @input="menu = false"></v-date-picker>
                    </v-menu>
                  </v-flex>
                  <v-flex xs12><v-divider></v-divider></v-flex>
                  <v-flex xs6 style="display: flex; align-items: center; justify-content: center;">
                    <p style="margin: 0;" v-cloak>Conocimiento</p>
                  </v-flex>
                  <v-flex xs6>
                    <v-rating :length="7" color="yellow darken-3" background-color="yellow lighten-3" hover open-delay="100" v-model="newItem.conocimiento"></v-rating>
                  </v-flex>
                  <v-flex xs12><v-divider></v-divider></v-flex>
                  <v-flex xs6 style="display: flex; align-items: center; justify-content: center;">
                    <p style="margin: 0;" v-cloak>Calidad de trabajo</p>
                  </v-flex>
                  <v-flex xs6>
                    <v-rating :length="7" color="yellow darken-3" background-color="yellow lighten-3" hover open-delay="100" v-model="newItem.calidad"></v-rating>
                  </v-flex>
                  <v-flex xs12><v-divider></v-divider></v-flex>
                  <v-flex xs6 style="display: flex; align-items: center; justify-content: center;">
                    <p style="margin: 0;" v-cloak>Productividad</p>
                  </v-flex>
                  <v-flex xs6>
                    <v-rating :length="7" color="yellow darken-3" background-color="yellow lighten-3" hover open-delay="100" v-model="newItem.productividad"></v-rating>
                  </v-flex>
                  <v-flex xs12><v-divider></v-divider></v-flex>
                  <v-flex xs6 style="display: flex; align-items: center; justify-content: center;">
                    <p style="margin: 0;" v-cloak>Trabajo en equipo</p>
                  </v-flex>
                  <v-flex xs6>
                    <v-rating :length="7" color="yellow darken-3" background-color="yellow lighten-3" hover open-delay="100" v-model="newItem.trabajo_en_equipo"></v-rating>
                  </v-flex>
                  <v-flex xs12><v-divider></v-divider></v-flex>
                  <v-flex xs6 style="display: flex; align-items: center; justify-content: center;">
                    <p style="margin: 0;" v-cloak>Compromiso</p>
                  </v-flex>
                  <v-flex xs6>
                    <v-rating :length="7" color="yellow darken-3" background-color="yellow lighten-3" hover open-delay="100" v-model="newItem.compromiso"></v-rating>
                  </v-flex>
                  <v-flex xs12><v-divider></v-divider></v-flex>
                  <v-flex xs6 style="display: flex; align-items: center; justify-content: center;">
                    <p style="margin: 0;" v-cloak>Relaciones humanas</p>
                  </v-flex>
                  <v-flex xs6>
                    <v-rating :length="7" color="yellow darken-3" background-color="yellow lighten-3" hover open-delay="100" v-model="newItem.relaciones_humanas"></v-rating>
                  </v-flex>
                  <v-flex xs12><v-divider></v-divider></v-flex>
                  <v-flex xs6 style="display: flex; align-items: center; justify-content: center;">
                    <p style="margin: 0;" v-cloak>Toma de decisiones</p>
                  </v-flex>
                  <v-flex xs6>
                    <v-rating :length="7" color="yellow darken-3" background-color="yellow lighten-3" hover open-delay="100" v-model="newItem.toma_de_decision"></v-rating>
                  </v-flex>
                  <v-flex xs12><v-divider></v-divider></v-flex>
                  <v-flex xs6 style="display: flex; align-items: center; justify-content: center;">
                    <p style="margin: 0;" v-cloak>Solución de problemas</p>
                  </v-flex>
                  <v-flex xs6>
                    <v-rating :length="7" color="yellow darken-3" background-color="yellow lighten-3" hover open-delay="100" v-model="newItem.solucion_de_problemas"></v-rating>
                  </v-flex>
                  <v-flex xs12><v-divider></v-divider></v-flex>
                  <v-flex xs6 style="display: flex; align-items: center; justify-content: center;">
                    <p style="margin: 0;" v-cloak>Asistencia y puntualidad</p>
                  </v-flex>
                  <v-flex xs6>
                    <v-rating :length="7" color="yellow darken-3" background-color="yellow lighten-3" hover open-delay="100" v-model="newItem.asistencia_puntualidad"></v-rating>
                  </v-flex>
                  <v-flex xs12><v-divider></v-divider></v-flex>
                  <v-flex xs6 style="display: flex; align-items: center; justify-content: center;">
                    <p v-cloak style="margin: 0; text-align: center;">Cumplimiento de normas y reglas</p>
                  </v-flex>
                  <v-flex xs6>
                    <v-rating :length="7" color="yellow darken-3" background-color="yellow lighten-3" hover open-delay="100" v-model="newItem.normas_reglas"></v-rating>
                  </v-flex>
                  <v-flex xs12><v-divider></v-divider></v-flex>
                  <v-flex xs1 offset-xs2>
                    <v-text-field type="number" class="text-xs-center" readonly label="Puntaje" v-model="newItem.puntaje"></v-text-field>
                  </v-flex>
                  <v-flex id="levelMessageContainer" xs6 offset-xs2>
                    <p v-cloak>{{ nivel }}</p>
                  </v-flex>
                </v-form>
                </v-layout>
              </v-container>
            </v-card-text>

            <v-card-actions>
              <v-spacer></v-spacer>
              <v-btn v-cloak color="blue darken-1" flat @click="close">Cancelar</v-btn>
              <v-btn v-cloak color="blue darken-1" flat @click="save" :disabled="canSubmit" >Guardar</v-btn>
            </v-card-actions>
          </v-card>
        </v-dialog>

        <v-dialog v-model="editDialog" max-width="500px">
          <v-card>
            <v-card-title>
              <span v-cloak class="headline">Listador evaluaciones</span>
            </v-card-title>
            <v-card-text>
              <v-container>            
                <v-data-table
                  :headers="editHeaders"
                  :items="editedItem.evaluations"
                  class="elevation-1"
                  no-data-text="No existen registros que indiquen que este trabajador haya sido evaluado"
                  rows-per-page-text="Elementos por página"
                >
                  <template slot="items" slot-scope="props">
                    <td class="text-xs-left">{{ props.item.id }}</td>
                    <td class="text-xs-left">{{ props.item.date }}</td>
                    <td class="justify-start layout">
                      <v-tooltip left>
                        <v-icon
                          slot="activator"
                          small
                          class="mr-2"
                          @click="editEvaluation(props.item)"
                          style="height: 100%; margin: auto 0;"
                        >
                          edit
                        </v-icon>
                        Editar
                      </v-tooltip>
                      <v-tooltip right>
                        <v-icon
                          slot="activator"
                          small
                          class="mr-2"
                          @click="deleteEvaluation(props.item)"
                          style="height: 100%; margin: auto 0;"
                        >
                          delete
                        </v-icon>
                        Eliminar
                      </v-tooltip>
                    </td>
                  </template>
                </v-data-table>
              </v-container>
            </v-card-text>
          </v-card>
        </v-dialog>
        <v-card>
          <v-data-table
            :headers="headers"
            :items="workers"
            class="elevation-1"
            :loading="loading"
            no-data-text="No hay registros"
            rows-per-page-text="Elementos por página"
          >
            <template slot="items" slot-scope="props">
              <td class="text-xs-left">{{ props.item.id }}</td>
              <td class="text-xs-left">{{ props.item.name }}</td>
              <td class="text-xs-left">{{ props.item.admission_date }}</td>
              <td class="text-xs-left">{{ props.item.position }}</td>
              <td class="text-xs-left">{{ props.item.area }}</td>
              <td class="justify-start layout">
                <v-tooltip left>
                  <v-icon
                    slot="activator"
                    small
                    class="mr-2"
                    @click="editItem(props.item)"
                    style="height: 100%; margin: auto 0;"
                  >
                    visibility
                  </v-icon>
                  Ver más
                </v-tooltip>
              </td>
            </template>
            <template slot="pageText" slot-scope="props">
              Mostrando elementos: {{ props.pageStart }} al {{ props.pageStop }} de {{ props.itemsLength }}
            </template>
          </v-data-table>
        </v-card>
      </v-flex>
    </v-layout>
  </v-container>
  </v-app>
</div>

<script src="https://vuejs.org/js/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script>
new Vue({
  el: '#app',
  data: {
    workers: [],
    headers: [
      {
        text: 'ID',
        align: 'left',
        sortable: true,
        value: 'id'
      },
      {text: 'Nombres', value: 'name', sortable: true},
      {text: 'Fecha de ingreso', value: 'admission_date', sortable: true },
      {text: 'Cargo', value:"position", sortable: true},
      {text: 'Area', value:"area", sortable: true},     
      {text: 'Acciones', value: "actions", sortable: false}
    ],
    editHeaders: [
      {
        text: 'ID',
        align: 'left',
        sortable: true,
        value: 'id'
      },
      {text: 'Fecha de evaluación', value: 'date', sortable: true},
      {text: 'Acciones', value: 'actions', sortable: false}
    ],
    loading: false,
    newItem: {
      conocimiento: 1,
      calidad: 1,
      productividad: 1,
      trabajo_en_equipo: 1,
      compromiso: 1,
      relaciones_humanas: 1,
      toma_de_decision: 1,
      solucion_de_problemas: 1,
      asistencia_puntualidad: 1,
      normas_reglas: 1,
      worker_id: null,
      puntaje: 1
    },
    niveles: [
      'Insuficiente',
      'Insuficiente a regular',
      'Regular',
      'Regular a normal',
      'Normal',
      'Normal a sobresaliente',
      'Sobresaliente'
    ],
    editedItem: {
      worker_id: null,
      evaluations: []
    },
    defaultEditedItem: {
      worker_id: null,
      evaluations: []
    },
    defaultNewItem:  {
      conocimiento: 1,
      calidad: 1,
      productividad: 1,
      trabajo_en_equipo: 1,
      compromiso: 1,
      relaciones_humanas: 1,
      toma_de_decision: 1,
      solucion_de_problemas: 1,
      asistencia_puntualidad: 1,
      normas_reglas: 1,
      worker_id: null,
      puntaje: 0
    },
    selectWorkers: [],
    dialog: false,
    editDialog: false,
    menu: false,
    editmode: false,
    rules: {
      required: value => !!value || 'Este campo es requerido.',
      maxLength: value => value.length < 10 || 'STAPH'
    }
  },
  methods: {
    moment() {
      return moment();
    },
    load() {
      this.loading = true;
      axios.get('workers/readWithEvaluations')
      .then(response => {
        this.loading = false;
        this.workers = response.data.workers
      })
      .catch(error => {
        this.loading = false;
        console.log(error)
      })

      this.newItem = this.defaultNewItem;

      if(this.editedItem.worker_id != null)
      {
        let data = new FormData();
        data.append('worker_id',this.editedItem.worker_id);
        this.loading = true;
        axios.post('evaluations/read',data)
        .then(response => {
          this.loading = false;
          this.editedItem.evaluations = response.data.evaluations;
          this.editDialog = true;
        })
      }
    },
    editItem(item){
      this.editedItem.worker_id = item.id;
      let data = new FormData();
      data.append('worker_id',this.editedItem.worker_id);
      this.loading = true;
      axios.post('evaluations/read',data)
      .then(response => {
        this.loading = false;
        this.editedItem.evaluations = response.data.evaluations;
        this.editDialog = true;
      })
    },
    editEvaluation(item) {
      this.dialog = true;
      this.editmode = true;
      for(i in item)
      {
        if(i != "date")
        {
          item[i] = +item[i]
        }
      }
      this.newItem = Object.assign({ worker_id: this.editedItem.worker_id },item);
    },
    deleteEvaluation(item){
      Swal({
        title: '¿Estás seguro?',
        text: "¡La evaluación será eliminada para siempre!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: '¡Si! ¡eliminar!',
        cancelButtonText: '¡No! ¡cancelar!',
        reverseButtons: true
      }).then((result) => {
        if (result.value) {
          let data = new FormData();
          data.append('id',item.id);
          axios.post('evaluations/delete',data)
          .then(response => {
            if(response)
            {
              Swal(
                '¡Eliminado!',
                'La evaluación ha sido eliminada.',
                'success'
              ).then(response => {
                this.load();
              })
            }
            else
            {
              Swal(
                'Error',
                'Ha ocurrido un error.',
                'warning'
              ).then(response => {
                this.load();
              })
            }
          })
        } else if (
          result.dismiss === Swal.DismissReason.cancel
        ) {
          Swal(
            'Cancelado',
            'la evaluación no fue eliminada.',
            'success'
          )
        }
      })
    },
    save(){
      let data = new FormData();
      data.append('evaluation_form',JSON.stringify(this.newItem));
      if(this.$refs.form.validate())
      {
        if(!this.editmode)
        {
          axios.post('evaluations/create',data)
          .then(response => {
            swal('Excelente!','Evaluacion realizada correctamente','success')
            .then(val => {
              this.load();
              this.dialog = false;
            })
          })
          .catch(error => {
            this.load();
          })
        }
        else
        {
          axios.post('evaluations/update',data)
          .then(response => {
            Swal('Bien!','La evaluación ha sido actualizada correctamente','success')
            .then(val => {
              this.load();
              this.dialog = false;
            })
          })
        }
      }
    },
    close(){
      this.dialog = false;
      setTimeout(() => {
        this.newItem = Object.assign({}, this.defaultNewItem);
        this.$refs.form.reset()
      }, 300)
    }
  },
  created() {
    this.load();
    axios.get('workers/read')
      .then(response => {
        this.selectWorkers = response.data.workers
      })
      .catch(error => {
        console.log(error)
      })
  },
  computed: {
    puntajeProm() {
      this.newItem.puntaje = (this.newItem.conocimiento + this.newItem.calidad + this.newItem.productividad + this.newItem.trabajo_en_equipo + this.newItem.compromiso + this.newItem.relaciones_humanas + this.newItem.toma_de_decision + this.newItem.solucion_de_problemas + this.newItem.asistencia_puntualidad + this.newItem.normas_reglas) / 10;
      return Math.round(this.newItem.puntaje)
    },
    nivel() {
      return this.niveles[this.puntajeProm - 1]
    },
    formTitle () {
      return 1 ? 'Nueva evaluacion' : 'Editar evaluaciones'
    },
    canSubmit() {
      return false;
    }
  },
  watch: {
    dialog (val) {
      val || this.close()
    }
  }
});


</script>