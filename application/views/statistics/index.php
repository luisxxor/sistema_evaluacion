<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/css/inputmask.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>

<style>
  [v-cloak], [v-cloak] > * {
    display: none;
  }

  .echarts {
    width: 100%!important;
  }

  #noDataMessage {
    margin: 1em 0; text-align: center;
  }

</style>

<div id="app">
  <v-app>
  <v-container>
    <h3 v-cloak class="text-xs-center display-2">Estadísticas</h3>
    <v-layout>
      <v-flex lg8 offset-lg2 md10 offset-md1 sm12>
        <v-card class="mt-5">
          <v-card-title v-cloak>
            <h6 class="title" style="margin: 0 auto;">Mejor trabajador por area y rango de fecha</h6>
          </v-card-title>
          <v-card-text>
            <v-layout wrap>
              <v-flex md8 offset-md2 xs12>
                <v-autocomplete
                  no-data-text="Lo sentimos, no hay areas registradas"
                  label="Area"
                  :items="areas"
                  item-text="name"
                  item-value="id"
                  v-model="params.area_id"
                  @change="updateGraph"
                ></v-autocomplete>
              </v-flex>
              <v-divider style="flex: 100%; margin-bottom: 1em"></v-divider>
              <v-flex sm12 lg5 md6 style="display: flex; justify-content: center; flex-wrap: wrap;">
                <p v-cloak class="subheading text-xs-center" style="flex: 100%">Desde</p>
                <v-date-picker
                  locale="es"
                  v-model="params.from_date"
                  :max="params.to_date"
                  @change="updateGraph"
                >
                </v-date-picker>
              </v-flex>
              <v-flex sm12 lg5 md6 offset-lg2 style="display: flex; justify-content: center; flex-wrap: wrap;">
                <p v-cloak class="subheading text-xs-center" style="flex: 100%">Hasta</p>
                <v-date-picker
                  locale="es"
                  v-model="params.to_date"
                  :min="params.from_date"
                  :max="moment().format()"
                  @change="updateGraph"
                >
                </v-date-picker>
              </v-flex>
            </v-layout>
            <v-divider class="my-3"></v-divider>
            <v-layout>
              <v-flex xs12 v-if="statistics.length > 0">
                <v-chart :autoresize="true" :options="options"/>
              </v-flex>
              <v-flex xs12 v-else>
                <p id="noDataMessage" v-cloak>{{ noDataMessage }}</p>
              </v-flex>
            </v-layout>
          </v-card-text>
        </v-card>
      </v-flex>
    </v-layout>
  </v-container>
  </v-app>
</div>

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/echarts@4.1.0/dist/echarts.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-echarts@4.0.0"></script>

<script>
  Vue.component('v-chart', VueECharts)

  new Vue({
    el: '#app',
    data: {
      areas: [],
      params: {
        area_id: null,
        from_date: moment().subtract(1, 'months').startOf('month').format('YYYY-MM-DD'),
        to_date: moment().format('YYYY-MM-DD')
      },
      statistics: [],
      options: {
        color: ['#3398DB'],
        tooltip : {
          trigger: 'axis',
          axisPointer: {
            type : 'line'
          }
        },
        grid: {
          left: '3%',
          right: '4%',
          bottom: '3%',
          containLabel: true
        },
        xAxis: [
          {
            type: 'category',
            data: [],
            axisTick: {
              alignWithLabel: true
            }
          }
        ],
        yAxis: [
          {
            type : 'value'
          }
        ],
        series: [
          {
            name:'Puntaje',
            type:'bar',
            barWidth: '60%',
            data: []
          }
        ]
      }
    },
    methods: {
      moment() {
        return moment();
      },
      updateGraph() {
        if(this.params.area_id)
        {
          let data = new FormData();
          data.append('statistics_params',JSON.stringify(this.params));
          axios.post('statistics/getStatistics',data)
          .then(response =>{
            this.statistics = response.data.statistics
          })
        }
      }
    },
    created() {
      axios.get('areas/read')
      .then(response => {
        this.areas = response.data.areas
      })
    },
    computed: {
      noDataMessage() {
        if(this.params.area_id)
        {
          let selected_area_name = this.areas.find(val => val.id == this.params.area_id).name;
          return `No hay registros de evaluaciones de trabajadores en el area de ${selected_area_name} realizadas en el periodo ${this.moment(this.params.from_date).format('DD/MM/YYYY')} - ${this.moment(this.params.to_date).format('DD/MM/YYYY')}`
        }
        else
        {
          return `Seleccione un area.`
        }
      },
      usernames() {
        return this.statistics.length > 0 ? this.statistics.map(val => val.name) : []
      },
      indexes() {
        return this.statistics.length > 0 ? this.statistics.map(val => Math.round(+val.promedio*100)/100) : []
      },
      bestWorker() {
        return this.statistics.length > 0 ? this.statistics[0] : null
      }
    },
    watch: {
      statistics(val) {
        this.options.xAxis[0].data = this.usernames;
        this.options.series[0].data = this.indexes;
        return val
      }
    }
  });
</script>