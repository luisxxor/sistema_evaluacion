<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statistics extends CI_Controller {
  
	public function __construct() {
		parent::__construct();
		$this->load->model('Statistics_model', 'statistics');
		$this->load->library('form_validation');
	}
	
	public function index()
	{
		if ($this->session->userdata('is_authenticated') == FALSE) {
			redirect('users/login');
		} else {
			$data['title'] = 'Estadísticas';
			$data['content'] = 'statistics/index';
			$this->load->view('template', $data);
		}
    }

    public function getStatistics()
    {
        if ($this->session->userdata('is_authenticated') == FALSE) {
			echo json_encode(['status' => '403','message' => 'Permission Denied']);
			return null;
		}

        $data = json_decode($this->input->post('statistics_params'),true);
        
        $result = $this->statistics->get($data);

/*         if(count($result) > 0)
		{ */
			echo json_encode(['status' => '201', 'statistics' => $result, 'data' => $data]);
/* 		}
		else
		{
			echo json_encode(['status' => '500', 'statistics' => [], 'data' => $data ]);
		} */
    }
}
