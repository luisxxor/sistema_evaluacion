<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Evaluations extends CI_Controller {
  
	public function __construct() {
		parent::__construct();
		$this->load->model('Evaluation_model', 'evaluation');
		$this->load->library('form_validation');
	}
	
	public function index()
	{
		if ($this->session->userdata('is_authenticated') == FALSE) {
			redirect('users/login');
		} else {
			$data['title'] = 'Listado de Evaluaciones';
			$data['content'] = 'evaluations/index';
			$this->load->view('template', $data);
		}
	}

	public function read()
	{
		if ($this->session->userdata('is_authenticated') == FALSE) {
			echo json_encode(['status' => '403','message' => 'Permission Denied']);
			return null;
		}

		$worker_id = $this->input->post('worker_id');

		$data = $this->evaluation->getEvaluationsFromUser($worker_id);

		echo json_encode([
			'evaluations' => $data
		]);
	}
	
	public function create() {
		if ($this->session->userdata('is_authenticated') == FALSE) {
			echo json_encode(['status' => '403','message' => 'Permission Denied']);
			return null;
		}

		$data = json_decode($this->input->post('evaluation_form'),true);

		$result = $this->evaluation->form_insert($data);

		if($result > 0)
		{
			echo json_encode(['status' => '201', 'message' => 'Evaluacion creada exitosamente']);
		}
		else
		{
			echo json_encode(['status' => '500', 'message' => 'Evaluacion no creada, ha ocurrido un error']);
		}
	}

	public function update() {
		if ($this->session->userdata('is_authenticated') == FALSE) {
			echo json_encode(['status' => '403','message' => 'Permission Denied']);
			return null;
		}

		$data = json_decode($this->input->post('evaluation_form'),true);

		$result = $this->evaluation->form_update($data);

		if($result > 0)
		{
			echo json_encode(['status' => '200', 'message' => 'Evaluación actualizada exitosamente']);
		}
		else
		{
			echo json_encode(['status' => '500', 'message' => 'Evaluación no actualizada, ha ocurrido un error', 'response' => $result]);
		}
	}

	public function delete() {
		if ($this->session->userdata('is_authenticated') == FALSE) {
			echo json_encode(['status' => '403','message' => 'Permission Denied']);
			return null;
		}

		$id = $this->input->post('id');

		$result = $this->evaluation->delete($id);

		if($result > 0)
		{
			echo json_encode(['status' => '200', 'message' => 'Evaluación eliminada correctamente']);
		}
		else
		{
			echo json_encode(['status' => '500', 'message' => 'Evaluación no eliminada, ha ocurrido un error', 'response' => $result]);
		}
	}
}
