<?php
class Statistics_model extends CI_Model {

    public function get($data) {
        $this->db->select('w.id as id, w.name as name, w.area_id as area_id, AVG(e.puntaje) as promedio');
        $this->db->from('evaluations as e');
        $this->db->join('workers as w','e.worker_id = w.id','left');
        $this->db->where('w.area_id',$data['area_id']);
        $this->db->where('e.date >=',$data['from_date']);
        $this->db->where('e.date <=',$data['to_date']);
        $this->db->group_by('w.id');
        $this->db->order_by('promedio desc');
        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }
}