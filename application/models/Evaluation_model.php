<?php
class Evaluation_model extends CI_Model {

    public function form_insert($data){
        $this->db->insert('evaluations', $data);
        return $this->db->affected_rows();
    }

    public function form_update($data) {
        $this->db->update('evaluations',$data,array('id' => $data['id']));
        return $this->db->affected_rows();
    }

    public function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('evaluations');

        return $this->db->affected_rows();
    }

    public function getEvaluationsFromUser($id) {
        $query = "
            e.id as id,
            e.date as date,
            e.conocimiento as conocimiento,
            e.calidad as calidad,
            e.productividad as productividad,
            e.trabajo_en_equipo as trabajo_en_equipo,
            e.compromiso as compromiso,
            e.relaciones_humanas as relaciones_humanas,
            e.toma_de_decision as toma_de_decision,
            e.solucion_de_problemas as solucion_de_problemas,
            e.asistencia_puntualidad as asistencia_puntualidad,
            e.normas_reglas as normas_reglas,
            e.puntaje as puntaje
        ";

        $this->db->select($query);
        $this->db->from('evaluations e');
        $this->db->where("e.worker_id = $id");
        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }
}